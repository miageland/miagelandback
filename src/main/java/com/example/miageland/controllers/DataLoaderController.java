package com.example.miageland.controllers;
import com.example.miageland.dataloader.DataLoaderService;
import com.example.miageland.exceptions.GaugeErrorException;
import com.example.miageland.model.Gauge;
import com.example.miageland.services.GaugeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/dataLoad")
public class DataLoaderController{

    @Autowired
    private DataLoaderService dataLoaderService;

    @GetMapping
    public void updateGauge() throws GaugeErrorException {
        dataLoaderService.firstRun();
    }


}
