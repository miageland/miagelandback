package com.example.miageland.controllers;

import com.example.miageland.model.DailyStats;
import com.example.miageland.services.DailyStatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/dailyStats")
public class DailyStatsController {

    @Autowired
    private DailyStatsService dailyStatsService;

    @GetMapping("/{date}")
    public ResponseEntity<DailyStats> getDailyStats(@PathVariable LocalDate date) {
        Optional<DailyStats> dailyStats = dailyStatsService.getDailyStatsByDate(date);
        return dailyStats.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public List<DailyStats> getAllDailyStats() {
        return dailyStatsService.getAllDailyStats();
    }

}
