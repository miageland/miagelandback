package com.example.miageland.controllers;

import com.example.miageland.model.Attraction;
import com.example.miageland.model.GeneralStats;
import com.example.miageland.model.GeneralStatsCategory;
import com.example.miageland.services.GeneralStatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/generalStats")
public class GeneralStatsController {

    @Autowired
    private GeneralStatsService generalStatsService;

    @GetMapping("/{category}")
    public ResponseEntity<GeneralStats> getGeneralStats(@PathVariable GeneralStatsCategory category) {
        Optional<GeneralStats> generalStats = generalStatsService.getGeneralStatsByCategory(category);
        return generalStats.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public List<GeneralStats> getAllGeneralStats() {
        return generalStatsService.getAllGeneralStats();
    }
}
