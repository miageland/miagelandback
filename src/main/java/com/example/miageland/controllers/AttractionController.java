package com.example.miageland.controllers;

import com.example.miageland.model.Attraction;
import com.example.miageland.services.AttractionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/attractions")
public class AttractionController {
    @Autowired
    private AttractionService attractionService;

    public AttractionController(AttractionService attractionService) {
        this.attractionService = attractionService;
    }

    @GetMapping
    public List<Attraction> getAllAttractions() {
        return attractionService.getAllAttractions();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Attraction> getAttraction(@PathVariable Long id) {
        Optional<Attraction> attraction = attractionService.getAttractionById(id);
        return attraction.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public Attraction createAttraction(@RequestBody Attraction attraction) {
        return attractionService.createAttraction(attraction);
    }

    @PatchMapping("/{id}")
    public Attraction updateAttraction(@PathVariable Long id, @RequestBody Attraction attraction) {
        return attractionService.updateAttraction(id, attraction);
    }

    @DeleteMapping("/{id}")
    public void deleteAttraction(@PathVariable Long id) {
        attractionService.deleteAttraction(id);
    }
}
