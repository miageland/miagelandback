package com.example.miageland.controllers;

import com.example.miageland.exceptions.TransitionNotPossibleException;
import com.example.miageland.model.Ticket;
import com.example.miageland.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tickets")
public class TicketController {

    @Autowired
    private TicketService ticketService;


    @GetMapping("/{id}")
    public ResponseEntity<Ticket> getTicket(@PathVariable Long id) {
        Optional<Ticket> ticket = ticketService.getTicketById(id);
        return ticket.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public Ticket createTicket(@RequestBody Ticket ticket) {
        return ticketService.createTicket(ticket);
    }

    @PostMapping("/batch")
    public String createTickets(@RequestBody Ticket[] tickets) {
        for (Ticket ticket : tickets) {
            ticketService.createTicket(ticket);
        }
        return "Success";
    }

    @PostMapping("/generate")
    public String generateTickets(@RequestBody Ticket[] tickets) throws TransitionNotPossibleException {
        return ticketService.generateTickets(tickets);
    }

    @PatchMapping("/{id}")
    public Ticket updateTicket(@PathVariable Long id, @RequestBody Ticket ticket) throws TransitionNotPossibleException {
        return ticketService.updateTicket(id, ticket);
    }

    @DeleteMapping("/{id}")
    public void deleteTicket(@PathVariable Long id) {
        ticketService.deleteTicket(id);
    }

    @GetMapping
    public List<Ticket> getAllTickets() {
        return ticketService.getAllTickets();
    }
}
