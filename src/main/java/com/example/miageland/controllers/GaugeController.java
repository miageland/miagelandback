package com.example.miageland.controllers;

import com.example.miageland.exceptions.GaugeErrorException;
import com.example.miageland.model.Gauge;
import com.example.miageland.services.GaugeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/gauge")
public class GaugeController {

    @Autowired
    private GaugeService gaugeService;

    @GetMapping
    public ResponseEntity<Gauge> getGauge() {
        Optional<Gauge> gauge = gaugeService.getGauge();
        return gauge.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PatchMapping
    public Gauge updateGauge(@RequestBody Gauge gauge) throws GaugeErrorException {
        return gaugeService.updateGauge(gauge.getMaxVisitors());
    }


}
