package com.example.miageland.controllers;

import com.example.miageland.classes.LoginForm;
import com.example.miageland.model.Ticket;
import com.example.miageland.model.TicketCountByVisitorDTO;
import com.example.miageland.model.Visitor;
import com.example.miageland.services.TicketService;
import com.example.miageland.services.VisitorService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/visitors")
public class VisitorController {
    @Autowired
    private VisitorService visitorService;

    @Autowired
    private TicketService ticketService;


    @GetMapping("/{id}")
    public ResponseEntity<Visitor> getVisitor(@PathVariable Long id) {
        Optional<Visitor> visitor = visitorService.getVisitorById(id);
        return visitor.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public Visitor createVisitor(@RequestBody Visitor visitor) {

        return visitorService.createVisitor(visitor);
    }

    @PutMapping("/{id}")
    public Visitor updateVisitor(@PathVariable Long id, @RequestBody Visitor visitor) {
        return visitorService.updateVisitor(id, visitor);
    }

    @DeleteMapping("/{id}")
    public void deleteVisitor(@PathVariable Long id) {
        visitorService.deleteVisitor(id);
    }

    @GetMapping
    public List<Visitor> getAllVisitors() {
        return visitorService.getAllVisitors();
    }

    @GetMapping("/{id}/tickets")
    public List<Ticket> getTicketsByVisitorId(@PathVariable Long id) {
        return ticketService.getTicketsByVisitorId(id);
    }

    @GetMapping("/ticketsCount")
    public List<TicketCountByVisitorDTO> getTicketCountsByVisitor() {
        return visitorService.getTicketCountsByVisitor();
    }
}
