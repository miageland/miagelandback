package com.example.miageland.controllers;

import com.example.miageland.classes.LoginForm;
import com.example.miageland.model.Employee;
import com.example.miageland.model.Visitor;
import com.example.miageland.services.EmployeeService;
import com.example.miageland.services.VisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private VisitorService visitorService;

    @PostMapping("/employee")
    public Optional<Employee> loginEmployee(@RequestBody LoginForm loginForm) {
        return employeeService.loginEmployee(loginForm);
    }
    @PostMapping("/visitor")
    public Optional<Visitor> loginVisitor(@RequestBody LoginForm loginForm) {
        return visitorService.loginVisitor(loginForm);
    }



}
