package com.example.miageland.exceptions;

public class GaugeErrorException extends Exception{
    public GaugeErrorException(String message) {
        super(message);
    }

}
