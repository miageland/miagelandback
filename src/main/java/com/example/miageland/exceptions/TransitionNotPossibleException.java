package com.example.miageland.exceptions;

public class TransitionNotPossibleException extends Exception {
    public TransitionNotPossibleException(String message) {
        super(message);
    }
}
