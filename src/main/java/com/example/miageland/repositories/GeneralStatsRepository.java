package com.example.miageland.repositories;

import com.example.miageland.model.GeneralStats;
import com.example.miageland.model.GeneralStatsCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GeneralStatsRepository extends JpaRepository<GeneralStats, GeneralStatsCategory> {
}
