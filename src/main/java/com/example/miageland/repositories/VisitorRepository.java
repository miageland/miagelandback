package com.example.miageland.repositories;

import com.example.miageland.model.Employee;
import com.example.miageland.model.TicketCountByVisitorDTO;
import com.example.miageland.model.TicketState;
import com.example.miageland.model.Visitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface VisitorRepository extends JpaRepository<Visitor, Long> {

    @Query("SELECT new com.example.miageland.model.TicketCountByVisitorDTO(v.lastName" +
            ", v.firstName, COUNT(t.ticketNumber))" +
            "FROM Ticket t INNER JOIN t.visitor v " +
            "WHERE t.state IN (:states) " +
            "GROUP BY v.lastName, v.firstName")
    List<TicketCountByVisitorDTO> getTicketCountsByVisitor(@Param("states") List<TicketState> states);

    Optional<Visitor> findByMail(String mail);

    Optional<Visitor> findByMailAndPassword(String mail, String password);
}
