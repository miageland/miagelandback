package com.example.miageland.repositories;

import com.example.miageland.model.DailyStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface DailyStatsRepository extends JpaRepository<DailyStats, LocalDate> {

    @Query("SELECT d FROM DailyStats d WHERE d.date > CURRENT_DATE")
    List<DailyStats> getDailyStatsOfComingDays();

}
