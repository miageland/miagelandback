package com.example.miageland.repositories;

import com.example.miageland.model.Gauge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GaugeRepository extends JpaRepository<Gauge, String> {
}
