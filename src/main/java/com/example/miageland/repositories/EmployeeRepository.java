package com.example.miageland.repositories;

import com.example.miageland.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Optional<Employee> findByMail(String mail);

    Optional<Employee> findByMailAndPassword(String mail, String password);

}
