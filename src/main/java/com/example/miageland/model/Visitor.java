package com.example.miageland.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Visitor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String lastName;
    private String firstName;
    private String password;
    @Column(unique = true)
    private String mail;
    

    public Visitor(String lastName, String firstName,String password, String mail) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.password = password;
        this.mail = mail;
    }
}
