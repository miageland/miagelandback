package com.example.miageland.model;

public enum TicketState {
    notPAID,
    PAID,
    USED,
    CANCELLED
}
