package com.example.miageland.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TicketCountByVisitorDTO {
    private String lastName;
    private String firstName;
    private Long ticketCount;

    public TicketCountByVisitorDTO(String lastName, String firstName, Long ticketCount) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.ticketCount = ticketCount;
    }
}