package com.example.miageland.model;

public enum Role {
    EMPLOYEE,
    ADMINISTRATOR,
    DIRECTOR
}
