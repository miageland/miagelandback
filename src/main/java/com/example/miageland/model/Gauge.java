package com.example.miageland.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "singleton")})
public class Gauge {
    @Id
    private String id;

    private long maxVisitors;

    private boolean singleton = true;

    public Gauge() {
        this.id = "gauge";
        this.maxVisitors = 1000;
    }
}
