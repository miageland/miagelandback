package com.example.miageland.model;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class GeneralStats {

    @Id
    @Enumerated(EnumType.STRING)
    private GeneralStatsCategory category;
    private long amount;

    public GeneralStats(GeneralStatsCategory category) {
        this.category = category;
        this.amount = 0;
    }

    public void increaseStats() {
        this.amount++;
    }

    public void decreaseStats() {
        this.amount--;
    }
}
