package com.example.miageland.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ticketNumber;

    private LocalDate visitDate;

    private Float price;

    private TicketState state;

    @ManyToOne
    @JoinColumn(name = "visitor_id", referencedColumnName = "id")
    private Visitor visitor;

    public Ticket(LocalDate visitDate, Float price, TicketState state, Visitor visitor) {
        this.visitDate = visitDate;
        this.price = price;
        this.state = state;
        this.visitor = visitor;
    }


}
