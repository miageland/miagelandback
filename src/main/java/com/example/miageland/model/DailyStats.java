package com.example.miageland.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class DailyStats {

    @Id
    private LocalDate date;

    private long ticketsSold;

    private long visitorsCount;

    private double income;

    public DailyStats(LocalDate date) {
        this.date = date;
        this.ticketsSold = 0;
        this.income = 0;
        this.visitorsCount = 0;
    }

    public void increaseStats(float income) {
        this.ticketsSold++;
        this.visitorsCount++;
        this.income += income;
    }

    public void decreaseStats(float income) {
        this.visitorsCount--;
        this.income -= income;
    }
}
