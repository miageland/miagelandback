package com.example.miageland.dataloader;

import com.example.miageland.model.*;
import com.example.miageland.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class DataLoaderService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AttractionService attractionService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private TicketService ticketService;
    @Autowired
    private VisitorService visitorService;
    @Autowired
    private GeneralStatsService generalStatsService;
    @Autowired
    private GaugeService gaugeService;

    private Visitor visitor1;
    private Visitor visitor2;


    public void firstRun() {
        int rowCount = 0;

        try {
            // Vérifie le nombre de lignes dans la table 'attraction'
            rowCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM attraction", Integer.class);
        } catch (Exception e) {
            // Une exception sera lancée si la table n'existe pas
        }

        // Si la table n'existe pas ou ne contient pas de lignes, générez des données
        if (rowCount == 0) {
            generateData();
        }
    }

    private void generateData() {
        generateGauge();
        generateSampleGeneralStats();
        generateSampleAttractions();
        generateSampleEmployees();
        generateSampleVisitors();
        generateSampleTickets();
    }

    private void generateGauge() {
        gaugeService.createGauge(new Gauge());
    }

    private void generateSampleAttractions() {
        Attraction attraction1 = new Attraction("The Claw", true);
        Attraction attraction2 = new Attraction("The Giant Drop", false);
        attractionService.createAttraction(attraction1);
        attractionService.createAttraction(attraction2);
    }

    private void generateSampleEmployees() {
        Employee employee1 = new Employee("Torguet", "Patrice", "123", "patrice.torguet@toulouse.miage.fr", Role.ADMINISTRATOR);
        Employee employee2 = new Employee("Teyssie", "Cedric", "123", "cedric.teyssie@toulouse.miage.fr", Role.DIRECTOR);
        Employee employee3 = new Employee("Detrier", "Jonathan", "123", "jonathan.detrier@toulouse.miage.fr", Role.EMPLOYEE);
        employeeService.createEmployee(employee1);
        employeeService.createEmployee(employee2);
        employeeService.createEmployee(employee3);
    }

    private void generateSampleTickets() {
        LocalDate yesterdayDate = LocalDate.now().minusDays(1);
        LocalDate todayDate = LocalDate.now();
        LocalDate comingDate = LocalDate.now().plusDays(5);

        Ticket ticket1 = new Ticket(yesterdayDate, 5.0F, TicketState.notPAID, visitor1);
        Ticket ticket2 = new Ticket(todayDate, 3.0F, TicketState.notPAID, visitor2);
        Ticket ticket3 = new Ticket(comingDate, 12.0F, TicketState.notPAID, visitor2);
        Ticket ticket4 = new Ticket(comingDate, 8.0F, TicketState.notPAID, visitor1);
        ticketService.createTicket(ticket1);
        ticketService.createTicket(ticket2);
        ticketService.createTicket(ticket3);
        ticketService.createTicket(ticket4);
    }

    private void generateSampleVisitors() {
        visitor1 = new Visitor("Lussiez", "Yohan", "123", "yohan.lussiez@toulouse.miage.fr");
        visitor2 = new Visitor("Saurel", "Rémi", "123", "remi.saurel@toulouse.miage.fr");
        visitorService.createVisitor(visitor1);
        visitorService.createVisitor(visitor2);
    }

    private void generateSampleGeneralStats() {
        GeneralStats cancelledTickets = new GeneralStats(GeneralStatsCategory.CANCELLEDTICKETS);
        GeneralStats notPaidTickets = new GeneralStats(GeneralStatsCategory.NOTPAIDTICKETS);
        generalStatsService.createGeneralStats(cancelledTickets);
        generalStatsService.createGeneralStats(notPaidTickets);
    }
}
