package com.example.miageland.services;

import com.example.miageland.classes.LoginForm;
import com.example.miageland.model.Employee;
import com.example.miageland.model.TicketCountByVisitorDTO;
import com.example.miageland.model.TicketState;
import com.example.miageland.model.Visitor;
import com.example.miageland.repositories.VisitorRepository;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class VisitorService {

    @Autowired
    private  VisitorRepository visitorRepository;

    public Optional<Visitor> loginVisitor(LoginForm loginForm) {
        return visitorRepository.findByMailAndPassword(loginForm.getMail(), loginForm.getPassword());
    }

    public Visitor createVisitor(Visitor visitor) {
        return visitorRepository.save(visitor);
    }

    public Optional<Visitor> getVisitorById(Long id) {
        return visitorRepository.findById(id);
    }

    public List<Visitor> getAllVisitors() {
        return visitorRepository.findAll();
    }

    public Visitor updateVisitor(Long id, Visitor visitor) {
        Optional<Visitor> existingVisitor = visitorRepository.findById(id);
        if (existingVisitor.isPresent()) {
            Visitor updatedVisitor = existingVisitor.get();
            updatedVisitor.setLastName(visitor.getLastName());
            updatedVisitor.setFirstName(visitor.getFirstName());
            updatedVisitor.setMail(visitor.getMail());
            return visitorRepository.save(updatedVisitor);
        } else {
            return null;
        }
    }

    public List<TicketCountByVisitorDTO> getTicketCountsByVisitor() {
        return visitorRepository.getTicketCountsByVisitor(Arrays.asList(TicketState.notPAID
                , TicketState.PAID, TicketState.USED));
    }

    public void deleteVisitor(Long id) {
        visitorRepository.deleteById(id);
    }


}
