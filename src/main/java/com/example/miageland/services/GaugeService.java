package com.example.miageland.services;

import com.example.miageland.exceptions.GaugeErrorException;
import com.example.miageland.model.Gauge;
import com.example.miageland.repositories.GaugeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GaugeService {

    @Autowired
    private GaugeRepository gaugeRepository;

    @Autowired
    private DailyStatsService dailyStatsService;

    public Gauge createGauge(Gauge gauge) {
        return gaugeRepository.save(gauge);
    }

    public Optional<Gauge> getGauge() {
        return gaugeRepository.findById("gauge");
    }

    public Gauge updateGauge(Long maxVisitors) throws GaugeErrorException {
        if (dailyStatsService.isNewGaugeValid(maxVisitors) || maxVisitors == 0) {
            Optional<Gauge> existingGauge = gaugeRepository.findById("gauge");
            if (existingGauge.isPresent()) {
                Gauge updatedGauge = existingGauge.get();
                updatedGauge.setMaxVisitors(maxVisitors);
                return gaugeRepository.save(updatedGauge);
            }
        }
        else {
            throw new GaugeErrorException("There are too many visitors in the coming days for the new gauge");
        }
        return null;
    }
}
