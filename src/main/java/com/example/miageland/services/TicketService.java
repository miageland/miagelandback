package com.example.miageland.services;

import com.example.miageland.exceptions.TransitionNotPossibleException;
import com.example.miageland.model.GeneralStatsCategory;
import com.example.miageland.model.Ticket;
import com.example.miageland.model.TicketState;
import com.example.miageland.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class TicketService {
    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private GeneralStatsService generalStatsService;

    @Autowired
    private DailyStatsService dailyStatsService;

    public Ticket createTicket(Ticket ticket) {
        Ticket newTicket = new Ticket(ticket.getVisitDate(), ticket.getPrice(), TicketState.notPAID, ticket.getVisitor());
        generalStatsService.increaseGeneralStats(GeneralStatsCategory.NOTPAIDTICKETS);
        dailyStatsService.increaseDailyStats(ticket.getVisitDate(), ticket.getPrice());
        return ticketRepository.save(newTicket);
    }

    public String generateTickets(Ticket[] tickets) throws TransitionNotPossibleException {
        for(Ticket ticket : tickets) {
            Ticket newTicket = new Ticket(ticket.getVisitDate(), ticket.getPrice(), TicketState.notPAID, ticket.getVisitor());
            newTicket = ticketRepository.save(newTicket);
            TicketState aimedState = ticket.getState();
            ticket.setTicketNumber(newTicket.getTicketNumber());

            generalStatsService.increaseGeneralStats(GeneralStatsCategory.NOTPAIDTICKETS);
            dailyStatsService.increaseDailyStats(ticket.getVisitDate(), ticket.getPrice());
            // If the visit date in less than 7 days, we don't try to cancel it and just leave it as UNPAID
            if (aimedState == TicketState.CANCELLED && !LocalDate.now().plusDays(7).isAfter(ticket.getVisitDate())) {
                ticket.setState(TicketState.CANCELLED);
                this.updateTicket(ticket.getTicketNumber(), ticket);
            } else if (aimedState == TicketState.PAID) {
                ticket.setState(TicketState.PAID);
                this.updateTicket(ticket.getTicketNumber(), ticket);
            } else if (aimedState == TicketState.USED) {
                ticket.setState(TicketState.PAID);
                this.updateTicket(ticket.getTicketNumber(), ticket);
                ticket.setState(TicketState.USED);
                this.updateTicket(ticket.getTicketNumber(), ticket);
            }
        }
        return "Success";
    }

    public Optional<Ticket> getTicketById(Long id) {
        return ticketRepository.findById(id);
    }

    public List<Ticket> getAllTickets() {
        return ticketRepository.findAll();
    }

    public List<Ticket> getTicketsByVisitorId(Long visitorId) {
        return ticketRepository.findByVisitorId(visitorId);
    }

    public Ticket updateTicket(Long ticketNumber, Ticket ticket) throws TransitionNotPossibleException {
        Optional<Ticket> existingTicket = ticketRepository.findById(ticketNumber);
        if (existingTicket.isPresent()) {
            Ticket updatedTicket = existingTicket.get();
            updatedTicket.setState(changeTicketState(ticketNumber, ticket).getState());
            return ticketRepository.save(updatedTicket);
        } else {
            return null;
        }
    }

    public void deleteTicket(Long id) {
        ticketRepository.deleteById(id);
    }

    public Ticket changeTicketState(long ticketNumber, Ticket newTicket) throws TransitionNotPossibleException {
        Optional<Ticket> existingTicketOptional = ticketRepository.findById(ticketNumber);
        if (existingTicketOptional.isPresent()) {
            Ticket existingTicket = existingTicketOptional.get();
            // We look at the state wanted, and we check if the state transition is possible. If so we apply the new
            // state
            if ((newTicket.getState() == TicketState.PAID && existingTicket.getState() == TicketState.notPAID)
                    || (newTicket.getState() == TicketState.USED && existingTicket.getState() == TicketState.PAID)
                    || (newTicket.getState() == TicketState.CANCELLED && existingTicket.getState() != TicketState.USED)) {
                // Checking if the visit date is in less than 7 days

                if (newTicket.getState() == TicketState.CANCELLED
                        && LocalDate.now().plusDays(7).isAfter(existingTicket.getVisitDate())) {
                    throw new TransitionNotPossibleException("The cancellation is not possible because your visit date " +
                            "is in less than 7 days");
                }

                // We update the stats depending on the ticket's state workflow
                if (existingTicket.getState() == TicketState.notPAID) {
                    generalStatsService.decreaseGeneralStats(GeneralStatsCategory.NOTPAIDTICKETS);
                }
                if (newTicket.getState() == TicketState.CANCELLED) {
                    generalStatsService.increaseGeneralStats(GeneralStatsCategory.CANCELLEDTICKETS);
                    dailyStatsService.decreaseDailyStats(existingTicket.getVisitDate(), existingTicket.getPrice());
                }

                existingTicket.setState(newTicket.getState());
            } else {
                throw new TransitionNotPossibleException("The current state of the ticket is " + existingTicket.getState());
            }
        }
        return newTicket;
    }

}
