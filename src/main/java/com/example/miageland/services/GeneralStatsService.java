package com.example.miageland.services;

import com.example.miageland.model.GeneralStats;
import com.example.miageland.model.GeneralStatsCategory;
import com.example.miageland.repositories.GeneralStatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GeneralStatsService {

    @Autowired
    private GeneralStatsRepository generalStatsRepository;

    public GeneralStats createGeneralStats(GeneralStats generalStats) {
        return generalStatsRepository.save(generalStats);
    }

    public Optional<GeneralStats> getGeneralStatsByCategory(GeneralStatsCategory category) {
        return generalStatsRepository.findById(category);
    }

    public List<GeneralStats> getAllGeneralStats() {
        return generalStatsRepository.findAll();
    }

    public GeneralStats increaseGeneralStats(GeneralStatsCategory category) {
        Optional<GeneralStats> existingGeneralStats = generalStatsRepository.findById(category);
        if (existingGeneralStats.isPresent()) {
            GeneralStats updatedGeneralStats = existingGeneralStats.get();
            updatedGeneralStats.increaseStats();
            return generalStatsRepository.save(updatedGeneralStats);
        } else {
            return null;
        }
    }

    public GeneralStats decreaseGeneralStats(GeneralStatsCategory category) {
        Optional<GeneralStats> existingGeneralStats = generalStatsRepository.findById(category);
        if (existingGeneralStats.isPresent()) {
            GeneralStats updatedGeneralStats = existingGeneralStats.get();
            updatedGeneralStats.decreaseStats();
            return generalStatsRepository.save(updatedGeneralStats);
        } else {
            return null;
        }
    }

}
