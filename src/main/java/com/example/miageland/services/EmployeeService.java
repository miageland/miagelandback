package com.example.miageland.services;

import com.example.miageland.classes.LoginForm;
import com.example.miageland.model.Employee;
import com.example.miageland.model.Visitor;
import com.example.miageland.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    private TicketService ticketService;

    public Optional<Employee> loginEmployee(LoginForm loginForm) {
        return employeeRepository.findByMailAndPassword(loginForm.getMail(), loginForm.getPassword());
    }

    public Employee createEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    public Optional<Employee> getEmployeeById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public Employee updateEmployee(Long id, Employee employee) {
        Optional<Employee> existingEmployeeOptional = employeeRepository.findById(id);
        if (existingEmployeeOptional.isPresent()) {
            Employee updatedEmployee = existingEmployeeOptional.get();
            updatedEmployee.setRole(employee.getRole());
            return employeeRepository.save(updatedEmployee);
        } else {
            return null;
        }
    }

    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }


}
