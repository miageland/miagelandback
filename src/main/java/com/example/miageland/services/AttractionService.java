package com.example.miageland.services;

import com.example.miageland.model.Attraction;
import com.example.miageland.repositories.AttractionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AttractionService {

    private final AttractionRepository attractionRepository;

    public AttractionService(AttractionRepository attractionRepository) {
        this.attractionRepository = attractionRepository;
    }

    public Attraction createAttraction(Attraction attraction) {
        return attractionRepository.save(attraction);
    }

    public Optional<Attraction> getAttractionById(Long id) {
        return attractionRepository.findById(id);
    }

    public List<Attraction> getAllAttractions() {
        return attractionRepository.findAll();
    }

    public Attraction updateAttraction(Long id, Attraction attraction) {
        Optional<Attraction> existingAttraction = attractionRepository.findById(id);
        if (existingAttraction.isPresent()) {
            Attraction updatedAttraction = existingAttraction.get();
            updatedAttraction.setIsOpened(attraction.getIsOpened());
            return attractionRepository.save(updatedAttraction);
        } else {
            return null;
        }
    }

    public void deleteAttraction(Long id) {
        attractionRepository.deleteById(id);
    }
}
