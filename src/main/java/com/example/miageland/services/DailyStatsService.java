package com.example.miageland.services;

import com.example.miageland.model.DailyStats;
import com.example.miageland.repositories.DailyStatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class DailyStatsService {

    @Autowired
    private DailyStatsRepository dailyStatsRepository;

    public void createDailyStats(DailyStats dailyStats) {
        dailyStatsRepository.save(dailyStats);
    }

    public Optional<DailyStats> getDailyStatsByDate(LocalDate date) {
        return dailyStatsRepository.findById(date);
    }

    public List<DailyStats> getAllDailyStats() {
        return dailyStatsRepository.findAll();
    }

    public void increaseDailyStats(LocalDate date, float income) {
        Optional<DailyStats> existingDailyStats = dailyStatsRepository.findById(date);
        if (existingDailyStats.isPresent()) {
            DailyStats updatedDailyStats = existingDailyStats.get();
            updatedDailyStats.increaseStats(income);
            dailyStatsRepository.save(updatedDailyStats);
        } else {
            DailyStats newDailyStats = new DailyStats(date);
            newDailyStats.increaseStats(income);
            createDailyStats(newDailyStats);
        }
    }

    public void decreaseDailyStats(LocalDate date, float income) {
        Optional<DailyStats> existingDailyStats = dailyStatsRepository.findById(date);
        if (existingDailyStats.isPresent()) {
            DailyStats updatedDailyStats = existingDailyStats.get();
            updatedDailyStats.decreaseStats(income);
            dailyStatsRepository.save(updatedDailyStats);
        }
    }

    public boolean isNewGaugeValid(long maxVisitors) {
        boolean isValid = true;
        List<DailyStats> dailyStatsOfComingDays = dailyStatsRepository.getDailyStatsOfComingDays();
        int i = 0;
        while (isValid && i < dailyStatsOfComingDays.size()) {
            if (dailyStatsOfComingDays.get(i).getVisitorsCount() > maxVisitors) {
                isValid = false;
            }
            i++;
        }
        return isValid;
    }

}
